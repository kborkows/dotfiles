" vim:foldmethod=marker
" vim:foldlevel=0
" Install Plug if not already installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

" Plugins {{{
call plug#begin("~/.vim/plugged")

Plug 'nanotech/jellybeans.vim'
Plug 'airblade/vim-gitgutter'
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
Plug 'b4winckler/vim-angry'
Plug 'bling/vim-airline'  | Plug 'vim-airline/vim-airline-themes'
Plug 'ctrlpvim/ctrlp.vim' | Plug 'FelikZ/ctrlp-py-matcher'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/vim-easy-align'
Plug 'lfv89/vim-interestingwords'
Plug 'mileszs/ack.vim'
Plug 'scrooloose/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'tomtom/tcomment_vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-git'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-unimpaired'
Plug 'francoiscabrol/ranger.vim'

" Language specific
" c++
Plug 'vim-scripts/headerguard'
Plug 'derekwyatt/vim-fswitch'
Plug 'octol/vim-cpp-enhanced-highlight', { 'for': 'cpp' }

" js
Plug 'moll/vim-node', { 'for': 'javascript' }

call plug#end()
" }}}

" Misc {{{

filetype plugin indent on " Filetype detection

set autochdir                  " Change working directory to opened file's one
set hidden                     " Hide buffer (instead of closing)
set ttyfast                    " Improves drawing performance
set backspace=indent,eol,start " Make backspace work like it should
set noshowmatch                " Don't visually jump to matching bracket when typing closing one
set ttimeout
    set ttimeoutlen=50
set nowritebackup
set nobackup
set autoread                   " Reload file when changed outside vim
set noswapfile
set history=1000
set shell=bash                 " Use zsh shell when running external commands
set encoding=utf8
set display+=lastline
set exrc                       " source local .vimrc files
set secure                     " restrict usage of some commands in non-default .vimrc files

" }}}

" Colors {{{
"
syntax on
colorscheme jellybeans
" set background=dark

" }}}

" UI {{{

set number      " Show line numbers
set cursorline
set colorcolumn=100       " Highlight n-th column
set laststatus=2 " Always show statusline
set splitbelow
set splitright
set mouse=a " Enable mouse support
set showcmd " Show partial command in the last line of screen
set wildmenu
    set wildignore=*.o,*~,*.pyc,*/tmp/*,*.so,*.swp,*.zip
    set wildignorecase

" }}}

" Spaces, tabs and indenting {{{

set shiftwidth=4  " Indent width when using << and >>
set softtabstop=4 " How many spaces should TAB be replaced with
set shiftround    " Round indendation to multiples of shiftwidth when using << and >>
set expandtab     " When typing, replace TAB with spaces
set autoindent    " Copy indentation from previous line

" }}}

" Searching and substituting{{{

set hlsearch   " highlight all matches
set ignorecase " case-insensitive searching
set incsearch  " highlight matches when typing
set smartcase  " if there are uppercase chars in search pattern, make search case sensitive
set gdefault " by default replace matches in whole file, not only current line

" }}}

" Displaying text {{{

set scrolloff=5   " Scroll offset
set wrap          " wrap lines
    set linebreak " Respect words when wrapping
    set breakindent   " Indent wrapped text
    let &showbreak = '↳ '
set list
    set listchars=tab:⇢\ ,trail:·

" }}}

" General mappings{{{

" Make j/k work on wrapped lines
nnoremap j gj
nnoremap k gk

" Disable help keybind
noremap <F1> <ESC>
inoremap <F1> <NOP>

" Make selection remain after indenting
vnoremap > >gv
vnoremap < <gv

" Do not enter ex-mode with Q
nnoremap Q <nop>

" Use jk to exit insert mode
inoremap jk <esc>

" Select pasted text
nnoremap gp `[v`]

" }}}

" General leader mappings {{{

let mapleader="\<Space>"
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>e :e<CR>

" Edit/source vimrc
nnoremap <leader>ev :vs $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" }}}

" Plugin Mappings {{{

function! s:VSetSearch()
  let temp = @@
  norm! gvy
  let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
  let @@ = temp
endfunction

vnoremap * :<C-u>call <SID>VSetSearch()<CR>//<CR>
vnoremap # :<C-u>call <SID>VSetSearch()<CR>??<CR>

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

noremap <leader>b :Gblame<cr>

nnoremap <leader>d :YcmCompleter GoToImprecise<cr>
nnoremap <leader>yd :YcmCompleter GoTo<cr>
nnoremap <leader>yt :YcmCompleter GetType<cr>
nnoremap <leader>yf :YcmCompleter FixIt<cr>
nnoremap <leader>yc :YcmDiags <cr>

nmap <silent> <Leader>of :FSHere<cr>

nnoremap gr :Ack! "\b<c-r><c-w>\b" 

nnoremap <leader>f :tabe %<cr>

" }}}

" Plugin options {{{

" Airline
let g:airline_theme='badwolf'
let g:airline_left_sep=''
let g:airline_right_sep=''

" CtrlP
let g:ctrlp_by_filename = 1             " By default search filenames only
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_max_files=0
let g:ctrlp_lazy_update = 50            " Now, that's a really nice option! (reducing lag when typing in ctrlp)
let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_use_caching = 1
let g:ctrlp_clear_cache_on_exit = 1

" PyMatcher for CtrlP
if !has('python')
    echo 'In order to use pymatcher plugin, you need +python compiled vim'
else
    let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
endif

" YouCompleteMe
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_confirm_extra_conf = 0
let g:ycm_add_preview_to_completeopt = 0

if executable('ag')
    let g:ackprg = 'ag --vimgrep --smart-case'
endif
let g:ackhighlight = 1

let g:UltiSnipsExpandTrigger="<c-l>"
let g:UltiSnipsJumpForwardTrigger="<c-n>"
let g:UltiSnipsJumpBackwardTrigger="<c-p>"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "SbtsSnips"]

let g:cpp_class_scope_highlight = 1

function! g:HeaderguardName()
    return toupper(expand('%:t:gs/[^0-9a-zA-Z_]/_/g') . '_')
endfunction

" }}}

" Autocommands {{{

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

au VimResized * wincmd =

" }}}

" Abbreviations {{{

cnoreabbrev ag Ack!
cnoreabbrev aG Ack!
cnoreabbrev Ag Ack!
cnoreabbrev AG Ack!

cnoreabbrev Q qall

" }}}


