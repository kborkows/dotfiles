#!/bin/bash

dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
dotfiles_branch=$($dotfiles symbolic-ref --short HEAD)

dotfiles_status=$($dotfiles status --porcelain)
dotfiles_to_be_pushed=$($dotfiles cherry -v)

echo "$dotfiles_branch"
echo "$dotfiles_branch"

if [ -n "$dotfiles_status" ]; then
    case $BLOCK_BUTTON in
        1) notify-send "Unsaved dotfiles:" "$($dotfiles status -s)" ;;
    esac

    exit 33
fi

if [ -n "$dotfiles_to_be_pushed" ]; then
    case $BLOCK_BUTTON in
        1) notify-send "Unpushed dotfiles changes:" "$($dotfiles cherry -v | cut -d\  -f3-)" ;;
    esac

    exit 33
fi

