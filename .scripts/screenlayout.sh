#!/usr/bin/env bash

options=$(find ~/.config/autorandr/* -maxdepth 0 -type d | rev | cut -d/ -f1 | rev)
selection=$(echo "$options" | rofi -lines 15 -dmenu -i -p "Output configuration:")

# exit if selection was canceled
if [ $? -ne 0 ]; then
    exit 0
fi

output=$(autorandr 2>&1 --skip-options=gamma -l $selection)

if [ $? -ne 0 ]; then
    notify-send "Autorandr error" "$output"
fi

